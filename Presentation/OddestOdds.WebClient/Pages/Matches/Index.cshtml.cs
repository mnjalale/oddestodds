using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR.Client;
using OddestOdds.Domain.Resources;
using OddestOdds.WebClient.Enumerations;
using OddestOdds.WebClient.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.WebClient.Pages.Matches
{
    public class IndexModel : PageModel
    {
        private readonly IMatchService _matchService;
        public int ViewMode;
        public IEnumerable<MatchResource> Matches;

        public IndexModel(IMatchService matchService)
        {
            this._matchService = matchService;
        }

        public async Task OnGetAsync(int viewMode)
        {
            var matches = await _matchService.GetMatchesAsync();
            this.Matches = matches.OrderBy(c => c.MatchDate);
            this.ViewMode = viewMode;
        }

    }
}