using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR.Client;
using OddestOdds.Domain.Resources;
using OddestOdds.WebClient.Services.Contracts;
using System;
using System.Threading.Tasks;

namespace OddestOdds.WebClient.Pages.Matches
{
    public class EditModel : PageModel
    {
        private readonly IMatchService _matchService;
        private HubConnection _connection;

        public EditModel(IMatchService matchService)
        {
            this._matchService = matchService;
            _connection = Constants.GetHubConnection("/MatchesHub"); 
        }

        [BindProperty]
        public MatchResource Match { get; set; }

        public async Task<IActionResult> OnGetAsync(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            this.Match = await _matchService.GetMatchByIdAsync(id.Value);

            if (this.Match == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var res = await _matchService.UpdateMatchAsync(this.Match);

            if (res)
            {
                try
                {
                    await _connection.StartAsync();
                    await _connection.InvokeAsync("SendMessage", this.Match);
                }
                catch (Exception)
                {
                    Console.WriteLine("Errors occurred while connecting");
                }
                return Redirect($"/Matches/{Enumerations.MatchesViewMode.OddsHandler}");
            }
            else
            {
                return Page();
            }
        }
    }
}