﻿"user strict";

var connection = new signalR.HubConnectionBuilder().withUrl("https://localhost:44344/MatchesHub").build();

connection.on("UpdateOdds", function (match) {
    console.log(match);
    document.getElementById(match.id + "HomeOdds").innerHTML = match.homeOdds === 0 ? "-" : match.homeOdds.toFixed(2);
    document.getElementById(match.id + "DrawOdds").innerHTML = match.drawOdds === 0 ? "-" : match.drawOdds.toFixed(2);
    document.getElementById(match.id + "AwayOdds").innerHTML = match.awayOdds === 0 ? "-" : match.awayOdds.toFixed(2);  
});

connection.start().then(function () {
    console.log('Connection started');
}).catch(function (err) {
    return console.error(err.toString());
});
