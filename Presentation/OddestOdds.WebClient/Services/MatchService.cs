﻿using Newtonsoft.Json;
using OddestOdds.Domain.Resources;
using OddestOdds.WebClient.Services.Contracts;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OddestOdds.WebClient.Services
{
    public class MatchService : IMatchService
    {
        private HttpClient _httpClient;

        public MatchService()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = new Uri($"{Constants.BaseUrl}/api/matches");
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public async Task<MatchResource> GetMatchByIdAsync(Guid id)
        {
            var url = $"{Constants.BaseUrl}/api/matches/{id}";

            var resStr = await _httpClient.GetStringAsync(url);

            return JsonConvert.DeserializeObject<MatchResource>(resStr);
        }

        public async Task<IEnumerable<MatchResource>> GetMatchesAsync()
        {
            var resStr = await _httpClient.GetStringAsync("");
            var matches = JsonConvert.DeserializeObject<IEnumerable<MatchResource>>(resStr);
            return matches;
        }

        public async Task<bool> UpdateMatchAsync(MatchResource match)
        {
            var json = JsonConvert.SerializeObject(match);
            var stringContent = new StringContent(json, Encoding.UTF8, "application/json");

            var res = await _httpClient.PutAsync("", stringContent);

            return res.IsSuccessStatusCode;
        }
    }
}
