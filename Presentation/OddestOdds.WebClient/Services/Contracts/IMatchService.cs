﻿using OddestOdds.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OddestOdds.WebClient.Services.Contracts
{
    public interface IMatchService
    {
        Task<IEnumerable<MatchResource>> GetMatchesAsync();
        Task<MatchResource> GetMatchByIdAsync(Guid id);
        Task<bool> UpdateMatchAsync(MatchResource match);
    }
}
