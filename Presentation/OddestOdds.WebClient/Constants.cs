﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.WebClient
{
    public static class Constants
    {
        public static string BaseUrl => "https://localhost:44344";

        public static HubConnection GetHubConnection(string endPoint)
        {
            var connection = new HubConnectionBuilder()
              .WithUrl($"{Constants.BaseUrl}{endPoint}")
              .Build();

            connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await connection.StartAsync();
            };

            return connection;
        }
    }
}
