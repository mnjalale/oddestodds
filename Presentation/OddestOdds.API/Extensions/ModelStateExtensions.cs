﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;

namespace OddestOdds.API.Extensions
{
    public static class ModelStateExtensions
    {
        public static List<string> GetErrorMessages(this ModelStateDictionary dictionary)
        {
            return dictionary
                    .SelectMany(c => c.Value.Errors)
                    .Select(c => c.ErrorMessage)
                    .ToList();
        }
    }
}
