﻿using Microsoft.AspNetCore.SignalR;
using OddestOdds.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.API.Hubs
{
    public class MatchesHub : Hub
    {
        public async Task SendMessage(MatchResource match)
        {
            await Clients.All.SendAsync("UpdateOdds", match);
        }
    }
}
