﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using OddestOdds.API.Extensions;
using OddestOdds.API.Mapping.Extensions;
using OddestOdds.Application.Services.Contracts;
using OddestOdds.Domain.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.API.Controllers
{
    [Route("api/[controller]")]
    public class MatchesController : Controller
    {
        private readonly IMatchService _matchService;
        private readonly IMapper _mapper;

        public MatchesController(IMapper mapper, IMatchService matchService)
        {
            _mapper = mapper;
            _matchService = matchService;
        }

        [HttpGet("{id}")]
        public async Task<MatchResource> GetByIdAsync(Guid id)
        {
            var match = await _matchService.FindByIdAsync(id);
            return match.ToResource(_mapper);
        }


        [HttpGet]
        public async Task<IEnumerable<MatchResource>> GetAllAsync()
        {
            var matches = await _matchService.ListAsync();
            return matches.Select(c => c.ToResource(_mapper));
        }

        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody] MatchResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var matche = resource.ToEntity(_mapper);
            var result = await _matchService.AddAsync(matche);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var matcheResource = result.Model.ToResource(_mapper);
            return Ok(matcheResource);

        }

        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] MatchResource resource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var matche = resource.ToEntity(_mapper);

            var result = await _matchService.UpdateAsync(matche);

            if (!result.Success)
            {
                return BadRequest(result.Message);
            }

            var matcheResource = result.Model.ToResource(_mapper);
            return Ok(matcheResource);
        }


    }
}
