﻿using AutoMapper;
using OddestOdds.Domain.Entities;
using OddestOdds.Domain.Resources;

namespace OddestOdds.API.Mapping
{
    public class ResourceToEntityProfile : Profile
    {
        public ResourceToEntityProfile()
        {
            CreateMap<MatchResource, Match>();
            CreateMap<TeamResource, Team>();
        }
    }
}
