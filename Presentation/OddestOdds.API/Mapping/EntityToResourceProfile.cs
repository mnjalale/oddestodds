﻿using AutoMapper;
using OddestOdds.Domain.Entities;
using OddestOdds.Domain.Resources;

namespace OddestOdds.API.Mapping
{
    public class EntityToResourceProfile : Profile
    {
        public EntityToResourceProfile()
        {
            CreateMap<Match, MatchResource>();
            CreateMap<Team, TeamResource>();
        }
    }
}
