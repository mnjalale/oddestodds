﻿using AutoMapper;
using OddestOdds.Domain.Entities;
using OddestOdds.Domain.Resources;

namespace OddestOdds.API.Mapping.Extensions
{
    public static class EntityToResourceMappingExtension
    {
        public static MatchResource ToResource(this Match entity, IMapper mapper)
        {
            return mapper.Map<Match, MatchResource>(entity);
        }

        public static TeamResource ToResource(this Team entity, IMapper mapper)
        {
            return mapper.Map<Team, TeamResource>(entity);
        }
    }
}
