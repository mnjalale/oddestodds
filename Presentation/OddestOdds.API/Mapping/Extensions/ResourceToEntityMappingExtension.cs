﻿using AutoMapper;
using OddestOdds.Domain.Entities;
using OddestOdds.Domain.Resources;

namespace OddestOdds.API.Mapping.Extensions
{
    public static class ResourceToEntityMappingExtension
    {
        public static Match ToEntity(this MatchResource resource, IMapper mapper)
        {
            return mapper.Map<MatchResource, Match>(resource);
        }

        public static Team ToEntity(this TeamResource resource, IMapper mapper)
        {
            return mapper.Map<TeamResource, Team>(resource);
        }

    }
}
