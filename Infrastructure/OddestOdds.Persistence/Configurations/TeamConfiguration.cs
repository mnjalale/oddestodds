﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence.Configurations.Base;

namespace OddestOdds.Persistence.Configurations
{
    public class TeamConfiguration : ConfigurationBase<Team>
    {
        public override void Configure(EntityTypeBuilder<Team> builder)
        {
            base.Configure(builder);

            // Fields
            builder.ToTable("Teams");
            builder.Property(c => c.Name).IsRequired().HasMaxLength(50);
            builder.Property(c => c.Alias).IsRequired().HasMaxLength(50);

        }
    }
}
