﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence.Configurations.Base;

namespace OddestOdds.Persistence.Configurations
{
    public class MatchConfiguration : ConfigurationBase<Match>
    {
        public override void Configure(EntityTypeBuilder<Match> builder)
        {
            base.Configure(builder);

            // Fields
            builder.ToTable("Matches");
            builder.Property(c => c.HomeTeamId).IsRequired();
            builder.Property(c => c.AwayTeamId).IsRequired();
            builder.Property(c => c.MatchDate).IsRequired();
            builder.Property(c => c.HomeOdds).HasColumnType("decimal(18,2)");
            builder.Property(c => c.DrawOdds).HasColumnType("decimal(18,2)");
            builder.Property(c => c.AwayOdds).HasColumnType("decimal(18,2)");
            builder.HasOne(c => c.HomeTeam).WithMany(c => c.HomeMatches).HasForeignKey(c => c.HomeTeamId).OnDelete(DeleteBehavior.Restrict);
            builder.HasOne(c => c.AwayTeam).WithMany(c => c.AwayMatches).HasForeignKey(c => c.AwayTeamId).OnDelete(DeleteBehavior.Restrict);

        }
    }
}
