﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OddestOdds.Domain.Entities.Base;

namespace OddestOdds.Persistence.Configurations.Base
{
    public class ConfigurationBase<T> : IEntityTypeConfiguration<T> where T : EntityBase
    {
        public virtual void Configure(EntityTypeBuilder<T> builder)
        {
            builder.HasKey(c => c.Id);
            builder.HasQueryFilter(c => !c.IsDeleted);


        }
    }
}
