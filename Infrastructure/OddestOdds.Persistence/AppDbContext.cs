﻿using Microsoft.EntityFrameworkCore;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence.Configurations;
using System;

namespace OddestOdds.Persistence
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<Match> Matches { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new MatchConfiguration());
            builder.ApplyConfiguration(new TeamConfiguration());

            SeedData(builder);

            base.OnModelCreating(builder);
        }

        // Helper functions
        private void SeedData(ModelBuilder builder)
        {
            // Seed teams
            var manCityId = Guid.NewGuid();
            var liverpoolId = Guid.NewGuid();
            var chelseaId = Guid.NewGuid();

            builder.Entity<Team>().HasData(
                new Team() { Id = manCityId, Name = "Manchester City FC", Alias = "Man City" },
                new Team() { Id = liverpoolId, Name = "Liverpool FC", Alias = "Liverpool" },
                new Team() { Id = chelseaId, Name = "Chelsea FC", Alias = "Chelsea" }
                );

            // Seed matches
            builder.Entity<Match>().HasData(
                new Match() { HomeTeamId = manCityId, AwayTeamId = liverpoolId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 1, 14, 15, 0), TimeSpan.FromMinutes(0)) },
                new Match() { HomeTeamId = liverpoolId, AwayTeamId = chelseaId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 2, 14, 15, 0), TimeSpan.FromMinutes(0)) },
                new Match() { HomeTeamId = chelseaId, AwayTeamId = manCityId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 3, 14, 15, 0), TimeSpan.FromMinutes(0)) },
                new Match() { HomeTeamId = manCityId, AwayTeamId = chelseaId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 4, 14, 15, 0), TimeSpan.FromMinutes(0)) },
                new Match() { HomeTeamId = liverpoolId, AwayTeamId = manCityId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 5, 14, 15, 0), TimeSpan.FromMinutes(0)) },
                new Match() { HomeTeamId = chelseaId, AwayTeamId = liverpoolId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 6, 14, 15, 0), TimeSpan.FromMinutes(0)) }
                );
        }

        private void Get(ModelBuilder builder)
        {

        }

        private void SeedMatches(ModelBuilder builder)
        {

        }
    }
}
