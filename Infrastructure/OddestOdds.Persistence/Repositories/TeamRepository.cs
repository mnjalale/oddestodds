﻿using OddestOdds.Application.Repositories;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence.Repositories.Base;

namespace OddestOdds.Persistence.Repositories
{
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(AppDbContext context) : base(context)
        {
        }
    }
}
