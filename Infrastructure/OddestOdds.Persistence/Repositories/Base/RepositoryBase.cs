﻿using Microsoft.EntityFrameworkCore;
using OddestOdds.Application.Repositories.Base;
using OddestOdds.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace OddestOdds.Persistence.Repositories.Base
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : EntityBase
    {
        protected readonly AppDbContext Context;
        protected readonly DbSet<T> DbSet;

        public RepositoryBase(AppDbContext context)
        {
            Context = context;
            DbSet = context.Set<T>();
        }

        public virtual async Task AddAsync(T model)
        {
            await DbSet.AddAsync(model);
        }

        public void Update(T model)
        {
            DbSet.Update(model);
        }

        public async Task<IEnumerable<T>> GetAsync(
            Expression<Func<T, bool>> filter = null
            , Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
            , string includeProperties = ""
            , bool showDeleted = false)
        {
            IQueryable<T> models = DbSet;

            if (filter != null)
            {
                models = models.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                models = models.Include(includeProperty);
            }

            if (!showDeleted)
            {
                models = models.Where(c => !c.IsDeleted);
            }

            return orderBy != null ? await orderBy(models).ToListAsync() : await models.ToListAsync();
        }

        public async Task<T> FindByIdAsync(Guid id)
        {
            var model = await DbSet.FindAsync(id);
            return model;
        }

        public void Remove(T model)
        {
            model.IsDeleted = true;
            DbSet.Update(model);
        }

    }
}
