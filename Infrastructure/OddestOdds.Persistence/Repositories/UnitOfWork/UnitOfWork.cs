﻿using OddestOdds.Application.Repositories;
using OddestOdds.Application.Repositories.UnitOfWork;
using System.Threading.Tasks;

namespace OddestOdds.Persistence.Repositories.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private AppDbContext _context;

        public UnitOfWork(
            AppDbContext context,
            IMatchRepository matchRepository)
        {
            _context = context;
            MatchRepository = matchRepository;
        }

        public IMatchRepository MatchRepository { get; private set; }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
