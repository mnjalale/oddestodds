﻿using OddestOdds.Application.Repositories;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence.Repositories.Base;

namespace OddestOdds.Persistence.Repositories
{
    public class MatchRepository : RepositoryBase<Match>, IMatchRepository
    {
        public MatchRepository(AppDbContext context) : base(context)
        {
        }


    }
}
