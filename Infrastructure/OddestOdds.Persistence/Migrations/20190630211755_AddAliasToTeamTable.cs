﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OddestOdds.Persistence.Migrations
{
    public partial class AddAliasToTeamTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("4845e388-1959-4cdd-8206-fce100ceceec"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("5112911f-d7d7-4829-8950-779bd1702a44"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("5218defd-2506-4f02-b86f-bf677ed833a4"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("565a7e69-579d-47ea-8ab1-bd62b35785a5"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("903295cf-c669-4ce9-85a9-672bf30e2ac3"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("9c52d923-14bb-416b-b624-d87a531f51ca"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("752dc390-483f-4075-a518-bea7f6520a6a"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"));

            migrationBuilder.AddColumn<string>(
                name: "Alias",
                table: "Teams",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Alias", "IsDeleted", "Name" },
                values: new object[] { new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"), "Man City", false, "Manchester City FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Alias", "IsDeleted", "Name" },
                values: new object[] { new Guid("757643da-3caa-4887-92ed-de826c352248"), "Liverpool", false, "Liverpool FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "Alias", "IsDeleted", "Name" },
                values: new object[] { new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"), "Chelsea", false, "Chelsea FC" });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "AwayOdds", "AwayTeamId", "DrawOdds", "HomeOdds", "HomeTeamId", "IsDeleted", "MatchDate" },
                values: new object[,]
                {
                    { new Guid("857422fc-6542-4b0a-8df3-d0e15674aa38"), 0m, new Guid("757643da-3caa-4887-92ed-de826c352248"), 0m, 0m, new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"), false, new DateTimeOffset(new DateTime(2019, 8, 1, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("ff7c0c9f-d4ec-49da-82ba-d1cb7c2e86a4"), 0m, new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"), 0m, 0m, new Guid("757643da-3caa-4887-92ed-de826c352248"), false, new DateTimeOffset(new DateTime(2019, 8, 5, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("f12772a2-348d-475a-8699-0608f35b6920"), 0m, new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"), 0m, 0m, new Guid("757643da-3caa-4887-92ed-de826c352248"), false, new DateTimeOffset(new DateTime(2019, 8, 2, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("a9d30eb4-c264-4d60-b673-ae36647a6f43"), 0m, new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"), 0m, 0m, new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"), false, new DateTimeOffset(new DateTime(2019, 8, 3, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("bfd01df6-62c2-4662-ac66-904856f7cb7c"), 0m, new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"), 0m, 0m, new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"), false, new DateTimeOffset(new DateTime(2019, 8, 4, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("33c4967c-7d9b-43da-8f0d-09735d848b7c"), 0m, new Guid("757643da-3caa-4887-92ed-de826c352248"), 0m, 0m, new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"), false, new DateTimeOffset(new DateTime(2019, 8, 6, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("33c4967c-7d9b-43da-8f0d-09735d848b7c"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("857422fc-6542-4b0a-8df3-d0e15674aa38"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("a9d30eb4-c264-4d60-b673-ae36647a6f43"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("bfd01df6-62c2-4662-ac66-904856f7cb7c"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("f12772a2-348d-475a-8699-0608f35b6920"));

            migrationBuilder.DeleteData(
                table: "Matches",
                keyColumn: "Id",
                keyValue: new Guid("ff7c0c9f-d4ec-49da-82ba-d1cb7c2e86a4"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("090e79cb-7479-44ae-bb18-fb9747124a19"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("757643da-3caa-4887-92ed-de826c352248"));

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: new Guid("8e0a93c3-da11-4334-9463-08621e8aaf8c"));

            migrationBuilder.DropColumn(
                name: "Alias",
                table: "Teams");

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, "Manchester City FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, "Liverpool FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, "Chelsea FC" });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "AwayOdds", "AwayTeamId", "DrawOdds", "HomeOdds", "HomeTeamId", "IsDeleted", "MatchDate" },
                values: new object[,]
                {
                    { new Guid("565a7e69-579d-47ea-8ab1-bd62b35785a5"), 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), 0m, 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, new DateTimeOffset(new DateTime(2019, 8, 1, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("5218defd-2506-4f02-b86f-bf677ed833a4"), 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), 0m, 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, new DateTimeOffset(new DateTime(2019, 8, 5, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("9c52d923-14bb-416b-b624-d87a531f51ca"), 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), 0m, 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, new DateTimeOffset(new DateTime(2019, 8, 2, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("4845e388-1959-4cdd-8206-fce100ceceec"), 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), 0m, 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, new DateTimeOffset(new DateTime(2019, 8, 3, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("5112911f-d7d7-4829-8950-779bd1702a44"), 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), 0m, 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, new DateTimeOffset(new DateTime(2019, 8, 4, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("903295cf-c669-4ce9-85a9-672bf30e2ac3"), 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), 0m, 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, new DateTimeOffset(new DateTime(2019, 8, 6, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) }
                });
        }
    }
}
