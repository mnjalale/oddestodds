﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OddestOdds.Persistence.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    HomeTeamId = table.Column<Guid>(nullable: false),
                    AwayTeamId = table.Column<Guid>(nullable: false),
                    MatchDate = table.Column<DateTimeOffset>(nullable: false),
                    HomeOdds = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    DrawOdds = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AwayOdds = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Matches_Teams_AwayTeamId",
                        column: x => x.AwayTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Matches_Teams_HomeTeamId",
                        column: x => x.HomeTeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, "Manchester City FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, "Liverpool FC" });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "IsDeleted", "Name" },
                values: new object[] { new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, "Chelsea FC" });

            migrationBuilder.InsertData(
                table: "Matches",
                columns: new[] { "Id", "AwayOdds", "AwayTeamId", "DrawOdds", "HomeOdds", "HomeTeamId", "IsDeleted", "MatchDate" },
                values: new object[,]
                {
                    { new Guid("565a7e69-579d-47ea-8ab1-bd62b35785a5"), 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), 0m, 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, new DateTimeOffset(new DateTime(2019, 8, 1, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("5218defd-2506-4f02-b86f-bf677ed833a4"), 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), 0m, 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, new DateTimeOffset(new DateTime(2019, 8, 5, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("9c52d923-14bb-416b-b624-d87a531f51ca"), 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), 0m, 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), false, new DateTimeOffset(new DateTime(2019, 8, 2, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("4845e388-1959-4cdd-8206-fce100ceceec"), 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), 0m, 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, new DateTimeOffset(new DateTime(2019, 8, 3, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("5112911f-d7d7-4829-8950-779bd1702a44"), 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), 0m, 0m, new Guid("752dc390-483f-4075-a518-bea7f6520a6a"), false, new DateTimeOffset(new DateTime(2019, 8, 4, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) },
                    { new Guid("903295cf-c669-4ce9-85a9-672bf30e2ac3"), 0m, new Guid("601a7f89-b8ac-4faf-8f71-9285365506bb"), 0m, 0m, new Guid("ecb64712-fc2c-472e-91e5-82f71bae7cea"), false, new DateTimeOffset(new DateTime(2019, 8, 6, 14, 15, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Matches_AwayTeamId",
                table: "Matches",
                column: "AwayTeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_HomeTeamId",
                table: "Matches",
                column: "HomeTeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
