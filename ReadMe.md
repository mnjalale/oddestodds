
# Oddest Odds
## About
The Oddest Odds application displays 1X2 odds for a tournament featuring the English Premier League top 4 clubs. These odds are displayed in real time to clients.

The application consists of two sections, the client screen (accessible through the "Punter" link) and the back office screen (accessible through the "Odds Handler" link)

## Features
### Clients
* Can view 1X2 odds in real time

### Odds Handler
* Can view all the current 1X2 odds
* Can remove odds by updating value to "0" or leaving the field empty
* Can update odds for the scheduled matches

## Running the source code
* The application has been written using ASP.Net Core 2 and should be run using Visual Studio Community 2019.
* The application uses a SQL Server database. The appropriate connection string should be set in the "appsettings.json" file in the OddestOdds.API project. Migrations will be applied automatically when the application runs.
* In visual studio, start up projects should be set to both OddestOdds.API and OddestOdds.WebClient



