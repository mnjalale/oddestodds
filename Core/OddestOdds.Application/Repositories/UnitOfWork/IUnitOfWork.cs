﻿using System.Threading.Tasks;

namespace OddestOdds.Application.Repositories.UnitOfWork
{
    public interface IUnitOfWork
    {
        IMatchRepository MatchRepository { get; }

        // Functions
        Task SaveChangesAsync();
    }
}
