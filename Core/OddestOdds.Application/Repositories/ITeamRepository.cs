﻿using OddestOdds.Application.Repositories.Base;
using OddestOdds.Domain.Entities;

namespace OddestOdds.Application.Repositories
{
    public interface ITeamRepository : IRepositoryBase<Team>
    {
    }
}
