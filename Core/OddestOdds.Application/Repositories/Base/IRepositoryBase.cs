﻿using OddestOdds.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace OddestOdds.Application.Repositories.Base
{
    public interface IRepositoryBase<T> where T : EntityBase
    {
        Task<IEnumerable<T>> GetAsync(
             Expression<Func<T, bool>> filter = null
            , Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null
            , string includeProperties = ""
            , bool showDeleted = false
        );
        Task<T> FindByIdAsync(Guid id);
        Task AddAsync(T model);
        void Update(T model);
        void Remove(T model);
    }
}
