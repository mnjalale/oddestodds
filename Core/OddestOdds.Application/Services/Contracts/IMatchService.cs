﻿using OddestOdds.Application.Services.Contracts.Base;
using OddestOdds.Domain.Entities;

namespace OddestOdds.Application.Services.Contracts
{
    public interface IMatchService : IServiceBase<Match>
    {
    }
}
