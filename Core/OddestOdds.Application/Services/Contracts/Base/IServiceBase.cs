using OddestOdds.Application.Services.Communication;
using OddestOdds.Domain.Entities.Base;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OddestOdds.Application.Services.Contracts.Base
{
    public interface IServiceBase<T> where T : EntityBase
    {
        Task<IEnumerable<T>> ListAsync();
        Task<T> FindByIdAsync(Guid id);
        Task<ServiceResponse<T>> AddAsync(T model);
        Task<ServiceResponse<T>> UpdateAsync(T model);
        Task<ServiceResponse<T>> DeleteAsync(Guid id);
    }
}
