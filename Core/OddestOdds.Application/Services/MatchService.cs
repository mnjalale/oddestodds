﻿using OddestOdds.Application.Repositories;
using OddestOdds.Application.Repositories.UnitOfWork;
using OddestOdds.Application.Services.Base;
using OddestOdds.Application.Services.Contracts;
using OddestOdds.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.Application.Services
{
    public class MatchService : ServiceBase<Match, IMatchRepository>, IMatchService
    {
        public MatchService(IUnitOfWork unitOfWork, IMatchRepository repository)
            : base(unitOfWork, repository)
        {
        }

        public override async Task<IEnumerable<Match>> ListAsync()
        {
            var matches = await UnitOfWork.MatchRepository.GetAsync(includeProperties: $"{nameof(Match.HomeTeam)},{nameof(Match.AwayTeam)}");
            return matches;

        }

        public override async Task<Match> FindByIdAsync(Guid id)
        {
            var matches = await UnitOfWork.MatchRepository
                                    .GetAsync(filter: c => c.Id == id
                                                , includeProperties: $"{nameof(Match.HomeTeam)},{nameof(Match.AwayTeam)}");
            return matches.SingleOrDefault();
        }
    }
}
