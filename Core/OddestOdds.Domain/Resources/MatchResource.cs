using System;
using System.ComponentModel.DataAnnotations;

namespace OddestOdds.Domain.Resources
{
    public class MatchResource
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Home Team Id is required.")]      
        public Guid HomeTeamId { get; set; }

        [Required(ErrorMessage = "Away Team Id is required.")]
        public Guid AwayTeamId { get; set; }

        [Required(ErrorMessage = "Match Date is required.")]
        public DateTimeOffset MatchDate { get; set; }

        [Display(Name = "Home Odds")]
        public decimal HomeOdds { get; set; }
        [Display(Name = "Draw Odds")]
        public decimal DrawOdds { get; set; }
        [Display(Name = "Away Odds")]
        public decimal AwayOdds { get; set; }

        public TeamResource HomeTeam { get; set; }
        public TeamResource AwayTeam { get; set; }
    }
}
