using System;
using System.ComponentModel.DataAnnotations;

namespace OddestOdds.Domain.Resources
{
    public class TeamResource
    {
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Team Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Team Alias is required.")]
        public string Alias { get; set; }
    }
}
