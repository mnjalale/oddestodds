﻿using OddestOdds.Domain.Entities.Base;
using System.Collections.Generic;

namespace OddestOdds.Domain.Entities
{
    public class Team : EntityBase
    {
        public string Name { get; set; }

        public string Alias { get; set; }

        public ICollection<Match> HomeMatches { get; set; } = new List<Match>();
        public ICollection<Match> AwayMatches { get; set; } = new List<Match>();
    }
}
