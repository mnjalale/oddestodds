﻿using OddestOdds.Domain.Entities.Base;
using System;

namespace OddestOdds.Domain.Entities
{
    public class Match : EntityBase
    {
        public Guid HomeTeamId { get; set; }
        public Guid AwayTeamId { get; set; }
        public DateTimeOffset MatchDate { get; set; }
        public decimal HomeOdds { get; set; }
        public decimal DrawOdds { get; set; }
        public decimal AwayOdds { get; set; }

        public Team HomeTeam { get; set; }
        public Team AwayTeam { get; set; }

    }
}
