﻿using System;

namespace OddestOdds.Domain.Entities.Base
{
    public abstract class EntityBase
    {
        public EntityBase()
        {
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
