﻿using Microsoft.EntityFrameworkCore;
using OddestOdds.Persistence;
using System;

namespace OddestOdds.Application.Tests
{
    public class TestBase
    {
        private bool useSqlite;

        public AppDbContext GetDbContext()
        {
            var builder = new DbContextOptionsBuilder<AppDbContext>();
            if (useSqlite)
            {
                // Use Sqlite DB.
                builder.UseSqlite("DataSource=:memory:", x => { });
            }
            else
            {
                // Use In-Memory DB.
                builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
            }

            var dbContext = new AppDbContext(builder.Options);
            if (useSqlite)
            {
                // SQLite needt to open connection to the DB
                // Not required for in-memory-database and MS SQL
                dbContext.Database.OpenConnection();
            }

            return dbContext;
        }

        public void UseSqlite()
        {
            useSqlite = true;
        }
    }
}
