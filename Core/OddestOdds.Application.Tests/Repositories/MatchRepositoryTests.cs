﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OddestOdds.Domain.Entities;
using OddestOdds.Persistence;
using OddestOdds.Persistence.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OddestOdds.Application.Tests.Repositories
{
    [TestClass]
    public class MatchRepositoryTests : TestBase
    {
        private MatchRepository _matchRepository;
        private TeamRepository _teamRepository;
        private AppDbContext _appDbContext;

        [TestInitialize]
        public void TestInitialize()
        {
            _appDbContext = GetDbContext();

            _matchRepository = new MatchRepository(_appDbContext);
            _teamRepository = new TeamRepository(_appDbContext);
        }

        [TestMethod]
        public async Task AddAsync()
        {
            // First add the teams
            var manCityId = Guid.NewGuid();
            var liverpoolId = Guid.NewGuid();

            var manCity = new Team() { Id = manCityId, Name = "Manchester City FC" };
            var liverpool = new Team() { Id = liverpoolId, Name = "Liverpool FC" };

            await _teamRepository.AddAsync(manCity);
            await _teamRepository.AddAsync(liverpool);
            await _appDbContext.SaveChangesAsync();

            manCity = await _teamRepository.FindByIdAsync(manCityId);
            liverpool = await _teamRepository.FindByIdAsync(liverpoolId);
            var savedTeams = await _teamRepository.GetAsync();
            Assert.IsNotNull(manCity);
            Assert.IsNotNull(liverpool);
            Assert.AreEqual(savedTeams.Count(), 2);

            // Then add the match
            var match = new Match() { HomeTeamId = manCityId, AwayTeamId = liverpoolId, MatchDate = new DateTimeOffset(new DateTime(2019, 8, 1, 14, 15, 0), TimeSpan.FromMinutes(0)) };
            await _matchRepository.AddAsync(match);
            await _appDbContext.SaveChangesAsync();

            var addedMatch = await _matchRepository.FindByIdAsync(match.Id);
            Assert.IsNotNull(addedMatch);

        }

    }
}
